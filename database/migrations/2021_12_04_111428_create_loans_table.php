<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->decimal('amount', 11, 2);
            $table->string('currency', 8)->default('USD')->comment('like USD or INR');
            $table->tinyInteger('loan_term')->comment('in months');
            $table->string('repayment_frequency', 16)->default('weekely');
            $table->decimal('processing_fee_percentage', 3, 2)->default(2)->comment('one-time fee from 1% to 6%');
            $table->decimal('interest_rate', 3, 2)->default(2)->comment('Between 1.5% to 4% monthly');
            $table->decimal('disbursed_amount', 11, 2)->comment('requested amount - processing fee');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
