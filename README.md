## About Mini-Aspire app

This mini aspire app consists of dummy version of a loan app so that the one can think about the systems and architecture the real project would have.

It is a laravel framework based api with postman collection sample included. It consists of following apis

-   Authentication - signin, register.
-   Loan - list, apply, check details.
-   User.
-   Repayment.

## App functionalities

-   It's a simple API that allows to handle user loans.
-   mainly consists of three entities (as of now): users, loans, and repayments.
-   This API allows simple use cases, which include registering/signing in a new user, creating a new loan for a user, with different attributes (e.g. duration, repayment frequency, interest rate etc.), and allowing a user to make repayments for the loan.
-   The app consists of common validation and checks for obvious errors. For example a user cannot make a repayment for a loan that’s already been repaid.

## Installation Instructions

Clone this project, Go to the folder application using cd command on your cmd or terminal

-   Run `composer install` on your cmd or terminal
    Copy .env.example file to .env on the root folder. You can type copy .env.example .env if using command prompt Windows or cp .env.example .env if using terminal, Ubuntu
-   Run `cp .env.example .env`
    Open your .env file and change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.
-   Run `php artisan key:generate`
-   Run `php artisan migrate`
-   Run `php artisan db:seed` to run seeders
-   Run `php artisan serve`

## Running APIs

-   First import postman collection located at the root of the project.
-   make sure `{{baseURL}}` value set in environment as `http://127.0.0.1:8000/api/` if running from serve command.
-   Run register api inside auth and note the token received in response
-   put this token in Authorisation tab of each api, select Bearer token and put the token value in the field
