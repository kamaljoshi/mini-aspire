<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RepaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $total_interest = $this->loans->amount * ($this->loans->interest_rate * $this->loans->loan_term / 100);
        $total_amount_repayable = $this->loans->amount + $total_interest;
        $repayment_per_installment = $total_amount_repayable / $this->loans->loan_term;

        return [
            'id' => (int)$this->id,
            'user' => $this->user->name,
            'total_amount_repayable' => number_format($total_amount_repayable, 2),
            'repayment_per_installment' => number_format($repayment_per_installment, 2),
            'currency' => $this->loans->currency,
            'repayment_method' => $this->repayment_method,
            'loan_term' => $this->loans->loan_term . ' ' . str_plural('week', $this->loans->loan_term),
            'repayment_paid' => $this->loans->repayments()->count(),
        ];
    }
}
