<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\RepaymentResource;
use App\Models\Loans as LoanModel;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status = [
            LoanModel::LOAN_STATUS_PENDING => 'Pending',
            LoanModel::LOAN_STATUS_APPROVED => 'Approved',
            LoanModel::LOAN_STATUS_REPAID => 'Repaid',
            LoanModel::LOAN_STATUS_REJECTED => 'Rejected',
        ];

        $total_interest = $this->amount * ($this->interest_rate * $this->loan_term / 100);
        $total_amount_repayable = $this->amount + $total_interest;
        $repayment_per_installment = $total_amount_repayable / $this->loan_term;

        return [
            'id' => (int)$this->id,
            'user' => $this->user->name,
            'amount' => number_format($this->amount, 2),
            'currency' => $this->currency,
            'loan_term' => $this->loan_term . ' ' . str_plural('month', $this->loan_term),
            'processing_fee_percentage' => $this->processing_fee_percentage,
            'interest_rate' => $this->interest_rate,
            'disbursed_amount' => number_format($this->disbursed_amount, 2),
            'total_interest' => number_format($total_interest, 2),
            'total_amount_repayable' => number_format($total_amount_repayable, 2),
            'repayment_per_installment' => number_format($repayment_per_installment, 2),
            'status' => $status[$this->status],
            'repayments' => RepaymentResource::collection($this->whenLoaded('repayments')),
        ];
    }
}
