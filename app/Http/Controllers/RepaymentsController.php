<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\BaseController as BaseController;
use App\Events\RepaymentProcessed;
use App\Http\Resources\RepaymentResource;
use App\Models\Loans;
use App\Models\Repayment;

class RepaymentsController extends BaseController
{
    public function store()
    {
        $rules = [
            'repayment_amount' => ['required', 'regex:/^\d*(\.\d{2})?$/'],
            'repayment_method' => 'required|string',
        ];

        $data = request()->only([
            'repayment_amount', 'repayment_method'
        ]);

        $response = $this->validateWithJson($data, $rules);

        // if validation passes, proceed to create the repayment
        if ($response === true) {
            // check if user has any approved (but not fully repaid) loan
            $loan = Loans::select(['id', 'amount', 'interest_rate', 'loan_term'])
                ->where('user_id', auth()->user()->id)
                ->where('status', Loans::LOAN_STATUS_APPROVED)
                ->first();

            // if approved (but not fully repaid) loan found, proceed to create a repayment for that loan
            if ($loan) {
                try {
                    $total_interest = $loan->amount * ($loan->interest_rate * $loan->loan_term / 100);
                    $total_amount_repayable = $loan->amount + $total_interest;
                    $repayment_per_installment = number_format($total_amount_repayable / $loan->loan_term, 2, '.', '');
                    $repayment_amount = number_format($data['repayment_amount'], 2, '.', '');               

                    if ($repayment_per_installment === $repayment_amount) {
                        $repayment = $loan->repayments()->create($data);
                        event(new RepaymentProcessed($repayment));

                        return $this->sendResponse(['repayment' => RepaymentResource::make($repayment)], 'Repayment created.');
                    }

                    return $this->sendError([],'You must pay a repayment amount of ' . number_format($repayment_per_installment, 2));
                } catch (\Exception $e) {
                    return $this->sendError($e,'Something went wrong in processing your payment! Please try again.');
                }
            }

            return $this->sendError([],'No unpaid loan found to make a repayment.', 401);
        }

        return $this->sendError($response, 'validation failed.');
    }
}
