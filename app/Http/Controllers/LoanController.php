<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Events\LoanProcessed;
use App\Models\Loans;
use App\Http\Resources\LoanResource;

class LoanController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\validation
     */
    public function index()
    {
        // listing all the loans user has availaed till date
        $loans = Loans::with('user')->where('user_id', auth()->user()->id)->paginate(10);
        return $this->sendResponse(['loans' => LoanResource::collection($loans)], 'Loan history');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\validation
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'amount' => 'required|integer',
            'loan_term' => 'required|integer',
            'currency' => 'required|string',            
            'interest_rate' => 'required|between:1,10',
        ];

        $validation = $this->validateWithJson(request()->only([
            'amount', 'loan_term', 'currency', 'interest_rate',
        ]), $rules);

        // create the loan if valid
        if ($validation === true) {
            try {
                // check if user already has a pending or approved (but not fully repaid) loan
                // considering a user can avail a single loan at a time
                $loan_exists = Loans::where('user_id', auth()->user()->id)
                    ->whereIn('status', [Loans::LOAN_STATUS_PENDING, Loans::LOAN_STATUS_APPROVED])
                    ->exists();

                // if no pending or approved (but not fully repaid) loan, create the loan
                if ($loan_exists === true) {
                    return $this->sendError([],'You already have a pending or unpaid loan.', 401);
                }

                $loan = Loans::create(request()->all());
                event(new LoanProcessed($loan)); // trigger email and other automated process

                return $this->sendResponse(['loan' => LoanResource::make($loan)], 'You have applied for the loan sucessfully.');

            } catch (\Exception $e) {
                return $this->sendError($e,'Something went wrong! Please try again.');
            }
        }

        return $this->sendError($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\validation
     */
    public function show($id)
    {
        //
        $loan = Loans::with('user', 'repayments')->find($id);
        if($loan===null){
            return $this->sendError([],'No record for requested loan', 401);
        }
        return $this->sendResponse(['loan' => LoanResource::make($loan)],'Requested Loan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\validation
     */
    public function update(Request $request, $id)
    {
        //to-do
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\validation
     */
    public function destroy($id)
    {
        //to-do
    }
}
