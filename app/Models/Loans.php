<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loans extends Model
{
    use HasFactory;

    public const LOAN_STATUS_PENDING = 0;
    public const LOAN_STATUS_APPROVED = 1;
    public const LOAN_STATUS_REPAID = 2;
    public const LOAN_STATUS_REJECTED = 3;

    protected $fillable = ['amount', 'loan_term', 'currency','interest_rate'];

    protected $attributes = [
        'processing_fee_percentage' => 2,//setting default value.
    ];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($loan) {
            $processing_fee = $loan->amount * ($loan->processing_fee_percentage / 100);

            $loan->user_id = auth()->user()->id;
            $loan->disbursed_amount = $loan->amount - $processing_fee;
            $loan->status = self::LOAN_STATUS_APPROVED;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repayments()
    {
        return $this->hasMany(Repayment::class);
    }
}
