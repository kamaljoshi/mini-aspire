<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
    use HasFactory;

    protected $fillable = ['repayment_amount', 'repayment_method'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($repayment) {
            $repayment->user_id = auth()->user()->id;
        });

        static::created(function ($repayment) {
            //if repayments reach the required number of term then change the loan status to complete repaid.
            if ($repayment->loans->loan_term === $repayment->loans->repayments()->count()) {
                $repayment->loans->status = Loans::LOAN_STATUS_REPAID;
                $repayment->loans->save();
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loans()
    {
        return $this->belongsTo(Loans::class);
    }
}
