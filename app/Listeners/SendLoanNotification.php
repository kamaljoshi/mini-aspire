<?php

namespace App\Listeners;

use App\Events\LoanProcessed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendLoanNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\LoanProcessed  $event
     * @return void
     */
    public function handle(LoanProcessed $event)
    {
        //
    }
}
