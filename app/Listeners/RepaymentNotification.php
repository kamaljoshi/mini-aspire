<?php

namespace App\Listeners;

use App\Events\RepaymentProcessed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RepaymentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\RepaymentProcessed  $event
     * @return void
     */
    public function handle(RepaymentProcessed $event)
    {
        //
    }
}
